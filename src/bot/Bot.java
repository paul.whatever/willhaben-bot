package bot;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Bot {
	
	WebDriver driver;
	JavascriptExecutor js;
	
	WebElement republish;
	WebDriverWait wait;
	
	byte page;
	String site;
	byte stage = 1;

	public Bot(WebDriver driver, JavascriptExecutor js) {
		this.driver = driver;
		this.js = js;
	}
	
	/**
	 * Log you in and decline the cookie request.
	 */
	public void login() {
		driver.findElement(By.id("email")).sendKeys(Properties.username);
		driver.findElement(By.id("password")).sendKeys(Properties.password);
		driver.findElement(By.name("login")).click();
		driver.findElement(By.id("didomi-notice-learn-more-button")).click();
		driver.findElement(By.className("standard-button")).click();
	}
	
	/**
	 * Republish every single item on a page.
	 * @throws InterruptedException TimeUnit Function depends on it.
	 */
	public void republish() throws InterruptedException {
		// TODO play around with the scrollIntoView() Function from JavaScript
		// TODO Exception Handling for js.executeScript("window.scrollBy(0,document.body.scrollHeight)"); since it won't work sometimes
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		// TODO play around with waits from Selenium
		TimeUnit.SECONDS.sleep(1);
		
		page = 1;
		site = String.format("https://www.willhaben.at/iad/myprofile/myadverts?page=%s", page);
		swapPage();
		
		// Loop through every item on current page
		while(ExpectedConditions.visibilityOf(republish) != null) {

		try {
		
		// Go through the individual steps for a product
		switch (stage) {
		case 1:
			// Press the republish button	
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			republish.click();
			stage++;
			break;
		case 2:
			// Confirm the details
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
			TimeUnit.SECONDS.sleep(1);
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			// WebElement for saveAndContinueId won't work due to stale Exception
			driver.findElement(By.id("saveAndContinueId")).click();
			stage++;
			break;
		case 3:
			// Confirm the pictures
			TimeUnit.SECONDS.sleep(1);
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
			TimeUnit.SECONDS.sleep(1);
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			driver.findElement(By.id("saveAndContinueId")).click();
			stage++;
			break;
		case 4:
			// Confirm the delivery
			TimeUnit.SECONDS.sleep(1);
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			driver.findElement(By.id("paylivery-select-checkbox")).click();
			driver.findElement(By.id("saveAndContinueId")).click();
			stage++;
			break;
		case 5:
			// Confirm the extra products
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
			TimeUnit.SECONDS.sleep(1);
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			driver.findElement(By.id("saveAndContinueId")).click();
			stage++;
			break;
		default:
			// Publish
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
			TimeUnit.SECONDS.sleep(1);
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			driver.findElement(By.name("_eventId_payAndPublish")).click();
			
			// Get back to the overview
			TimeUnit.SECONDS.sleep(1);
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			driver.get(site);
			
			// Check if there are any items left, otherwise swap page
			TimeUnit.SECONDS.sleep(1);
			swapPage();
			stage = 1;
			break;
			}
		} catch (org.openqa.selenium.UnhandledAlertException ex) {
			// Reload the whole site whenever the spare "Loading..." page appears
			driver.navigate().refresh();
			driver.switchTo().alert().accept();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
	
	/**
	 * Check if there is any item left to republish. Otherwise swaps the page.
	 * @throws InterruptedException TimeUnit Function depends on it.
	 */
	public  void swapPage() throws InterruptedException {
		try {
			republish = driver.findElement(By.name("republish"));
		} catch(NoSuchElementException ex) {
			page++;
			site = String.format("https://www.willhaben.at/iad/myprofile/myadverts?page=%s", page);
			driver.get(site);
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
			TimeUnit.SECONDS.sleep(1);
			republish = driver.findElement(By.name("republish"));
		}
	}
}