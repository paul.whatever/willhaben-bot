package bot;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Main {
	
	static WebDriver driver;
	static JavascriptExecutor js;
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", "resources/geckodriver");
		// Your preferred WebDriver
		driver = new FirefoxDriver();
		driver.get(Properties.url);
		js = (JavascriptExecutor) driver;
		
		Bot bot = new Bot(driver, js);
		
		// Log you in and decline the cookie request
		bot.login();
		// Republish every single item on a page
		bot.republish();
		
		// Quit the browser
		driver.quit();
	}
}